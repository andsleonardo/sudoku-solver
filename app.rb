require 'pry'
require 'csv'

require_relative 'lib/sudoku/app.rb'
require_relative 'lib/sudoku_solver/app.rb'

# solved, unsolved = [0, 0]

# DATASET = CSV.read('dataset.csv')

# DATASET[1..100].each do |row|
# # DATASET[1..DATASET.size].each do |row|
#   matrix = Array.new(9) { [] }
#   row[0].split('').each_with_index { |value, i| matrix[i/9] << value.to_i }
#   solved_grid = SudokuSolver.solve(SudokuGrid.new(matrix))
#   solved_grid == row[1] ? solved += 1 : unsolved += 1
# end

# puts 'SOLVED: ' + solved.to_s
# puts 'UNSOLVED: ' + unsolved.to_s

SudokuSolver::Solver.new
