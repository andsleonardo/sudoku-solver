module SudokuSolver
  module MatrixBuilder
    extend self

    def ask_for_puzzle
      puts('Which puzzle would you like me to solve?')
      puts('Available options: easy, medium, hard or expert.')
      gets.chomp.to_sym
    end
  end
end
