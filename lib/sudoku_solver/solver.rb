module SudokuSolver
  PUZZLES = {
    easy: CSV.read('./spec/files/easy_sudoku_grid.csv', converters: :numeric),
    medium: CSV.read('./spec/files/medium_sudoku_grid.csv', converters: :numeric),
    hard: CSV.read('./spec/files/hard_sudoku_grid.csv', converters: :numeric),
    expert: CSV.read('./spec/files/expert_sudoku_grid.csv', converters: :numeric)
  }

  class Solver
    include Sudoku
    include MatrixBuilder

    def initialize
      @grid = Grid.new(PUZZLES[ask_for_puzzle])

      solve
    end

    private

    def solve
      save_empty_cells_count

      calculate_candidates
      apply_single_possibility_rule

      # apply_strategy('exclusive_candidate_rule')
      apply_strategy('naked_twins_rule')

      unless impossible_to_solve? || done?
        solve
      else
        print_diagnostic
      end
    end

    def save_empty_cells_count
      @empty_cells_start = empty_cells_remaining
    end

    def calculate_candidates
      @grid.empty_cells.each(&:set_candidates)
    end

    def apply_strategy(rule)
      return if @grid.solved?

      send("apply_#{rule}".to_sym)
      apply_single_possibility_rule
    end

    def apply_single_possibility_rule
      @grid.empty_cells.each do |cell|
        next unless cell.has_single_candidate?

        cell.set_value(cell.candidates[0])
      end
    end

    # TODO: Review this rule
    def apply_exclusive_candidate_rule
      @grid.blocks.each do |block|
        options = block.candidates.flatten

        options.select { |candidate| options.count(candidate) == 1 }.each do |exclusive|
          solvable_cell = block.empty_cells.find { |cell| cell.candidates.include?(exclusive) }
          return unless solvable_cell

          solvable_cell.remove_candidates(solvable_cell.candidates - [exclusive])
        end
      end
    end

    def apply_naked_twins_rule
      %w[rows columns].each do |blocks|
        @grid.send(blocks.to_sym).each do |block|
          candidates = block.candidates.select { |candidates| candidates.size == 2 }
          uniq_candidates = candidates.uniq

          next if candidates == uniq_candidates

          twins = uniq_candidates.select { |uniq| candidates.count(uniq) == 2 }

          block.empty_cells.each do |cell|
            next if cell.candidates == twins

            cell.remove_candidates(twins)
          end
        end
      end
    end

    def impossible_to_solve?
      @empty_cells_start == empty_cells_remaining
    end

    def empty_cells_remaining
      @grid.empty_cells.size
    end

    def done?
      @grid.solved?
    end

    def print_diagnostic
      puts [
        'GRID: ' + @grid.values.flatten.map { |value| value.nil? ? 0 : value }.join,
        "TOTAL VALUES SUM: #{@grid.values.flatten.map { |value| value.nil? ? 0 : value }.sum}",
        "ALL BLOCKS SOLVED? #{@grid.blocks.map(&:solved?).all? { |r| r == true }}",
        "EMPTY CELLS REMAINING: #{empty_cells_remaining}",
        "GRID SOLVED? #{@grid.solved?}"
      ]
    end
  end
end

# def old_solve(grid)
#   # Brute force
#   if empty_cells_start == empty_cells_end
#     sorted_cells = grid.empty_cells.sort { |cell1, cell2| cell1.candidates.size <=> cell2.candidates.size }
#     break if sorted_cells.empty?
#     sorted_cells[0].set_value(sorted_cells[0].candidates[0])
#     grid.blocks.select { |block| !block.filled? }.map(&:solvable?).uniq == [true] ? next : break
#   end
# end
