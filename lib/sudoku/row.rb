module Sudoku
  class Row
    include Blockable

    attr_reader :grid, :position, :cells

    def initialize(grid, position, cells)
      @grid = grid
      @position = position
      @cells = cells
    end
  end
end
