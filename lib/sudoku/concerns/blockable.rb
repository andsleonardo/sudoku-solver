module Sudoku
  module Blockable
    def empty_cells
      self.cells.reject(&:filled?)
    end

    def values
      self.cells.map(&:value)
    end

    def candidates
      self.cells.map(&:candidates)
    end

    def missing_values
      (1..9).to_a - values
    end

    def solved?
      filled? && values.sum == ((1..9).to_a * multiplier).sum
    end

    def filled?
      values.compact.size == 9 * multiplier
    end

    def solvable?
      return false if solved?
      [values.compact, *missing_values].sum == ((1..9).to_a * multiplier).sum
    end

    def multiplier
      self.class == Grid ? 9 : 1
    end
  end
end
