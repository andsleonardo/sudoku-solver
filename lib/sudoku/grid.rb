module Sudoku
  class Grid
    include Blockable

    attr_reader :cells, :rows, :columns, :subgrids

    def initialize(matrix)
      parse_values(matrix)
      build_blocks_and_cells
    end

    def blocks
      rows + columns + subgrids
    end

    private

    def parse_values(matrix)
      @matrix = matrix.map.with_index do |row, i|
        row.map.with_index { |value, j| Cell.new(self, [i, j], value) }
      end
    end

    def build_blocks_and_cells
      build_cells
      build_rows
      build_columns
      build_subgrids
    end

    def build_cells
      @cells = @matrix.flatten
    end

    def build_rows
      @rows = @matrix.map.with_index { |row, i| Row.new(self, i, row) }
    end

    def build_columns
      @columns = []
      9.times do |i|
        @columns << Column.new(self, i, @matrix.map { |row| row[i] })
      end
    end

    def build_subgrids
      @subgrids = []
      (0...3).each do |y|
        (0...3).each do |x|
          subgrid = @matrix.slice(y*3, 3).map { |row| row.slice(x*3, 3) }.flatten
          @subgrids << Subgrid.new(self, [y, x], subgrid)
        end
      end
    end
  end
end
