module Sudoku
  class Subgrid
    include Blockable

    attr_reader :grid, :coordinates, :cells

    def initialize(grid, coordinates, cells)
      @grid = grid
      @coordinates = coordinates
      @cells = cells
    end
  end
end
