module Sudoku
  class Cell
    attr_reader :grid, :coordinates, :value, :candidates

    def initialize(grid, coordinates, value)
      @grid = grid
      @coordinates = coordinates
      @value = value.zero? ? nil : value
      @candidates = []
    end

    def set_value(value)
      @value = value
      clear_candidates
    end

    def set_candidates
      @candidates = (1..9).to_a - impossible_values
    end

    def clear_candidates
      @candidates = []
    end

    def solvable?
      !filled? && has_single_candidate?
    end

    def filled?
      !!value
    end

    def has_single_candidate?
      candidates.size == 1
    end

    def impossible_values
      blocks.map(&:values).flatten.compact.uniq
    end

    def remove_candidates(nums)
      @candidates -= [nums].flatten
    end

    def blocks
      [row, column, subgrid]
    end

    def row
      grid.rows[coordinates[0]]
    end

    def column
      grid.columns[coordinates[1]]
    end

    def subgrid
      grid.subgrids.select do |subgrid|
        coords = coordinates.map { |coord| coord / 3 }
        subgrid.coordinates[0] == coords[0] && subgrid.coordinates[1] == coords[1]
      end.first
    end
  end
end
